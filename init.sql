CREATE DATABASE IF NOT EXISTS trainingFinal;

USE trainingFinal;

CREATE TABLE IF NOT EXISTS Ingredient(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    ingredientPrice DECIMAL(19,2)
);

CREATE TABLE IF NOT EXISTS DishType(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255)
);

INSERT INTO Ingredient(id,name,ingredientPrice)
    VALUES
(1,'manzana',5.2),
(2,'cebolla',2.5),
(3,'sal',3.5),
(4,'limon',4.5),
(5,'pan',10.0),
(6,'yogurt',10.0),
(7,'agua',15.0);

INSERT INTO DishType(id,name) VALUES (1,'aperitvo'),(2,'postre');

CREATE TABLE IF NOT EXISTS Dish(
    id INT PRIMARY KEY AUTO_INCREMENT,
    idDishType INT,
    name VARCHAR(255),
    description TEXT,
    dishPrice DECIMAL(19,2),
    FOREIGN KEY(idDishType) REFERENCES DishType(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO Dish(id,idDishType,name,description,dishPrice)
    VALUES
(1,1,'ensalada de manzana','ensalada de manzana',40.0),
(2,1,'ensalada ligera','ensalada ligera',30);

CREATE TABLE IF NOT EXISTS IngredientXDish(
    id INT PRIMARY KEY AUTO_INCREMENT,
    idIngredient INT,
    idDish INT,
    quantity INT,
    FOREIGN KEY(idIngredient) REFERENCES Ingredient(id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY(idDish) REFERENCES Dish(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO IngredientXDish(id,idIngredient,idDish,quantity) VALUES
(1,1,1,3),
(2,6,1,1),
(3,2,2,3),
(4,3,2,2),
(5,4,2,2);

CREATE TABLE IF NOT EXISTS PurchaseOrder(
    id INT PRIMARY KEY AUTO_INCREMENT,
    idDish INT,
    totalPrice DECIMAL(19,2),
    FOREIGN KEY(idDish) REFERENCES Dish(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS OrderDetail(
    id INT PRIMARY KEY AUTO_INCREMENT,
    idPurchaseOrder INT,
    idDish INT,
    FOREIGN KEY(idPurchaseOrder) REFERENCES PurchaseOrder(id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY(idDish) REFERENCES Dish(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

DELIMITER $$
CREATE PROCEDURE SP_CREATE_DISH_TYPE(
    IN name VARCHAR(255)
)
BEGIN
    DECLARE idDishType INT;
    INSERT INTO DishType(name) VALUES (name);
    SET idDishType = LAST_INSERT_ID();
    SELECT * FROM DishType WHERE id = idDishType;
END$$

CREATE PROCEDURE SP_CREATE_INGREDIENT(
    IN name VARCHAR(255),
    IN ingredientPrice DECIMAL(19,2)
)
BEGIN
    DECLARE idIngredient INT;
    INSERT INTO Ingredient(name,ingredientPrice) VALUES (name,ingredientPrice);
    SET idIngredient = LAST_INSERT_ID();
    SELECT * FROM Ingredient WHERE id = idIngredient;
END$$

CREATE PROCEDURE SP_GET_WISHES_BY_INGREDIENT_ID(
    IN idIngredient INT
)
BEGIN
    SELECT
    	i.id as ingredient_id,
        i.name as ingredient_name,
        i.ingredientPrice,
        ixd.quantity,
        ixd.idDish,
        ixd.id as ixd_id,
        d.name as dish_name,
        d.dishPrice as dish_price,
        d.description as dish_description
    FROM Ingredient i
    INNER JOIN IngredientXDish ixd on i.id = ixd.idIngredient
    INNER JOIN Dish d on ixd.idDish = d.id
    WHERE i.id = idIngredient;
END$$

CREATE PROCEDURE SP_CREATE_DISH(
    IN input_data JSON
)
BEGIN
    DECLARE dish_name VARCHAR(255);
    DECLARE dish_description TEXT;
    DECLARE idDishType INT;
    DECLARE dish_id INT;
    DECLARE ingredient_count INT;
    DECLARE dish_price DECIMAL(19,2) DEFAULT 0;

    DECLARE i INT DEFAULT 0;

    SET dish_name = REPLACE(REPLACE(JSON_EXTRACT(input_data, '$.name'), '\\', ''), '""', '"');
    SET dish_description = REPLACE(REPLACE(JSON_EXTRACT(input_data,'$.description'), '\\', ''), '""', '"');
    SET idDishType = CAST(JSON_EXTRACT(input_data,'$.idDishType') AS SIGNED);
    SET ingredient_count = CAST(JSON_LENGTH(input_data, '$.ingredientsDTO') AS SIGNED);


    WHILE i < ingredient_count DO
        SET @ingredient_id_path = CONCAT('$.ingredientsDTO[', i, '].idIngredient');
        SET @ingredient_id_value = CAST(JSON_EXTRACT(input_data,@ingredient_id_path) AS SIGNED);

        SET @ingredient_quantity_path = CONCAT('$.ingredientsDTO[', i, '].quantity');
        SET @ingredient_quantity_value = CAST(JSON_EXTRACT(input_data,@ingredient_quantity_path) AS SIGNED);

        SET @ingredient_price = 0;

        SELECT
            ingredientPrice INTO @ingredient_price
        FROM Ingredient
        WHERE id = @ingredient_id_value;

        SET @subtotal = ROUND(@ingredient_price * @ingredient_quantity_value, 2);
        SET dish_price = dish_price + @subtotal;

        SET i = i + 1;
    END WHILE;

    INSERT INTO Dish(idDishType,name,description,dishPrice) VALUES
    (idDishType,SUBSTRING(dish_name, 2, LENGTH(dish_name) - 2),SUBSTRING(dish_description, 2, LENGTH(dish_description) - 2),dish_price);
    SET dish_id = LAST_INSERT_ID();

    SET i = 0;

    WHILE i < ingredient_count DO
        SET @ingredient_id_path = CONCAT('$.ingredientsDTO[', i, '].idIngredient');
        SET @ingredient_id_value = CAST(JSON_EXTRACT(input_data,@ingredient_id_path) AS SIGNED);

        SET @ingredient_quantity_path = CONCAT('$.ingredientsDTO[', i, '].quantity');
        SET @ingredient_quantity_value = CAST(JSON_EXTRACT(input_data,@ingredient_quantity_path) AS SIGNED);

        INSERT INTO IngredientXDish(idIngredient,idDish,quantity) VALUES
        (@ingredient_id_value,dish_id,@ingredient_quantity_value);
        SET i = i + 1;
    END WHILE;

    SELECT
    	d.id as dish_id,
        d.name as dish_name,
        d.dishPrice,
        d.description,
        d.idDishType,
        dt.name as dish_type_name
    FROM Dish d
    	INNER JOIN DishType dt on d.idDishType = dt.id
    WHERE d.id = dish_id;
END$$

CREATE PROCEDURE GET_INGREDIENTS_FROM_DISH(
    IN dish_id INT
)
BEGIN
    SELECT ixd.id as ixd_id,ixd.idIngredient,ixd.quantity,i.name as ingredient_name,i.ingredientPrice FROM IngredientXDish ixd
    INNER JOIN Dish d on d.id = ixd.idDish
    INNER JOIN Ingredient i on ixd.idIngredient = i.id
    where ixd.idDish = dish_id;
END$$
DELIMITER ;