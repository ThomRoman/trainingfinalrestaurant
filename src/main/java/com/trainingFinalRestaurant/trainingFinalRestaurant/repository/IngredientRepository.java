package com.trainingFinalRestaurant.trainingFinalRestaurant.repository;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.IngredientDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.IngredientXDish;

import java.util.List;

public interface IngredientRepository {
	List<Ingredient> getIngredients();
	Ingredient createIngredient(IngredientDTO ingredientDTO);
	Ingredient getIngredientById(int id);

	List<IngredientXDish> ingredientXDishes(int ingredientId);
}
