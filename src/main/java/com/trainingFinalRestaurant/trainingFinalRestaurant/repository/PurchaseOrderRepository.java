package com.trainingFinalRestaurant.trainingFinalRestaurant.repository;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.PurchaseOrderDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.PurchaseOrder;

import java.util.List;

public interface PurchaseOrderRepository {
	List<PurchaseOrder> getPurchaseOrders();
	PurchaseOrder getPurchaseOrderById(int id);
	PurchaseOrder createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO);
}
