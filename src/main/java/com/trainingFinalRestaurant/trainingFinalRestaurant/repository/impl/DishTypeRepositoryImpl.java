package com.trainingFinalRestaurant.trainingFinalRestaurant.repository.impl;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishTypeDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.DishTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class DishTypeRepositoryImpl implements DishTypeRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<DishType> getDishTypes() {
		String sql = "SELECT * FROM DishType";
		List<DishType> dishTypes = jdbcTemplate.query(sql,(rs, rowNum) -> mapToDishType(rs));
		return dishTypes;
	}


	public DishType mapToDishType(ResultSet rs) throws SQLException {
		return new DishType(rs.getInt("id"),rs.getString("name"));
	}

	@Override
	public DishType createDishType(DishTypeDTO dishTypeDTO) {
		String sql = "CALL SP_CREATE_DISH_TYPE(?)";
		DishType dishType = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToDishType(rs), dishTypeDTO.getName());
		return dishType;
	}

	@Override
	public DishType getDishTypeById(int id) {
		String sql = "SELECT * FROM DishType WHERE id = ?";
		DishType dishType = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToDishType(rs), id);
		return dishType;
	}
}
