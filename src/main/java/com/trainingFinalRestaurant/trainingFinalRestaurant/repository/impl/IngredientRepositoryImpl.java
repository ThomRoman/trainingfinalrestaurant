package com.trainingFinalRestaurant.trainingFinalRestaurant.repository.impl;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.IngredientDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Dish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.IngredientXDish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class IngredientRepositoryImpl implements IngredientRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Ingredient> getIngredients() {
		String sql = "SELECT * FROM Ingredient";
		List<Ingredient> ingredients = jdbcTemplate.query(sql,(rs, rowNum) -> mapToIngredient(rs));
		return ingredients;
	}

	@Override
	public Ingredient createIngredient(IngredientDTO ingredientDTO) {
		String sql = "CALL SP_CREATE_INGREDIENT(?,?)";
		Object[] params = new Object[] {
				ingredientDTO.getName(),
				ingredientDTO.getIngredientPrice()
		};
		Ingredient ingredient = jdbcTemplate.queryForObject(sql, params,
				(rs, rowNum) -> mapToIngredient(rs));

		return ingredient;
	}

	public Ingredient mapToIngredient(ResultSet rs) throws SQLException {
		var ingredient =  new Ingredient();
		ingredient.setId(rs.getInt("id"));
		ingredient.setName(rs.getString("name"));
		ingredient.setIngredientPrice(rs.getInt("ingredientPrice"));
		return ingredient;
	}

	@Override
	public Ingredient getIngredientById(int id) {
		String sql = "SELECT * FROM Ingredient WHERE id = ?";
		Ingredient ingredient = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToIngredient(rs), id);
		return ingredient;
	}

	@Override
	public List<IngredientXDish> ingredientXDishes(int ingredientId){
		String sql = "CALL SP_GET_WISHES_BY_INGREDIENT_ID(?)";
		List<IngredientXDish> ixds = jdbcTemplate.query(sql,(rs, rowNum) -> maptoIngredientXDish(rs), ingredientId);
		return ixds;
	}
	public IngredientXDish maptoIngredientXDish(ResultSet rs) throws SQLException {
		var ingredientXdish =  new IngredientXDish();
		var dish = new Dish();
		dish.setId(rs.getInt("idDish"));
		dish.setName(rs.getString("dish_name"));
		dish.setDishPrice(rs.getDouble("dish_price"));
		dish.setDescription(rs.getString("dish_description"));

		ingredientXdish.setId(rs.getInt("ixd_id"));
		ingredientXdish.setQuantity(rs.getInt("quantity"));
		ingredientXdish.setDish(dish);
		return ingredientXdish;
	}
}
