package com.trainingFinalRestaurant.trainingFinalRestaurant.repository.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Dish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.IngredientXDish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class DishRepositoryImpl implements DishRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Dish> getDishes() {
		String sql = "select d.id as dish_id,d.idDishType,d.name,d.description,d.dishPrice,dt.name as dish_type_name from Dish d inner join DishType dt on d.idDishType = dt.id";
		List<Dish> dishes = jdbcTemplate.query(sql,(rs, rowNum) -> mapToDish(rs));
		return dishes;
	}

	public Dish mapToDish(ResultSet rs) throws SQLException{
		var dish = new Dish();
		dish.setDishPrice(rs.getDouble("dishPrice"));
		dish.setId(rs.getInt("dish_id"));
		dish.setName(rs.getString("name"));
		dish.setDescription(rs.getString("description"));
		var dishType = new DishType();
		dishType.setId(rs.getInt("idDishType"));
		dishType.setName(rs.getString("dish_type_name"));
		dish.setDishType(dishType);
		return dish;
	}

	@Override
	public Dish createDish(DishDTO dishDTO){
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
		objectMapper.configure(JsonGenerator.Feature.QUOTE_NON_NUMERIC_NUMBERS, false);

		try {
			String inputJson = objectMapper.writeValueAsString(dishDTO);
			String sql = "CALL SP_CREATE_DISH(?)";

			Dish dish = jdbcTemplate.queryForObject(sql,(rs, rowNum) -> mapToDishForProcedure(rs),inputJson);
			return dish;
		}catch (Exception e){
			System.out.println(e.getMessage());
			return null;
		}

	}

	public Dish mapToDishForProcedure(ResultSet rs) throws SQLException {
		Dish dish = new Dish();
		DishType dishType = new DishType(
				rs.getInt("idDishType"),
				rs.getString("dish_type_name")
		);
		dish.setDishPrice(rs.getDouble("dishPrice"));
		dish.setId(rs.getInt("dish_id"));
		dish.setName(rs.getString("dish_name"));
		dish.setDescription(rs.getString("description"));
		dish.setDishType(dishType);
		return dish;
	}


	@Override
	public List<IngredientXDish> getIngredientsFromDish(int dishId){
		String sql = "CALL GET_INGREDIENTS_FROM_DISH(?)";
		List<IngredientXDish> ixds = jdbcTemplate.query(sql,(rs, rowNum) -> mapToIngredientsFromDish(rs), dishId);
		return ixds;
	}



	public IngredientXDish mapToIngredientsFromDish (ResultSet rs) throws SQLException {
		var ingredientXdish =  new IngredientXDish();
		ingredientXdish.setId(rs.getInt("ixd_id"));
		ingredientXdish.setQuantity(rs.getInt("quantity"));
		var ingredient = new Ingredient();
		ingredient.setId(rs.getInt("idIngredient"));
		ingredient.setName(rs.getString("ingredient_name"));
		ingredient.setIngredientPrice(rs.getDouble("ingredientPrice"));
		ingredientXdish.setIngredient(ingredient);
		return ingredientXdish;
	}

	@Override
	public Dish getDishById(int id) {
		String sql = "select d.id as dish_id,d.idDishType,d.name,d.description,d.dishPrice,dt.name as dish_type_name from Dish d inner join DishType dt on d.idDishType = dt.id WHERE d.id = ?";
		Dish dish = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToDish(rs), id);
		return dish;
	}
}
