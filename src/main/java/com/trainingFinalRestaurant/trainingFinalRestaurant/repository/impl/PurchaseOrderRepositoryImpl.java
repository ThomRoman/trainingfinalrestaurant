package com.trainingFinalRestaurant.trainingFinalRestaurant.repository.impl;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.PurchaseOrderDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.PurchaseOrder;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.PurchaseOrderRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class PurchaseOrderRepositoryImpl implements PurchaseOrderRepository {
	@Override
	public List<PurchaseOrder> getPurchaseOrders() {
		return null;
	}

	@Override
	public PurchaseOrder getPurchaseOrderById(int id) {
		return null;
	}

	@Override
	public PurchaseOrder createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) {
		return null;
	}
}
