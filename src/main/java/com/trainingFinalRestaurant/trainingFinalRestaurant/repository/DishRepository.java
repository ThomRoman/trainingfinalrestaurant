package com.trainingFinalRestaurant.trainingFinalRestaurant.repository;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Dish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.IngredientXDish;

import java.util.List;

public interface DishRepository {
	List<Dish> getDishes();
	Dish createDish(DishDTO dishDTO);
	Dish getDishById(int id);
	List<IngredientXDish> getIngredientsFromDish(int dishId);
}
