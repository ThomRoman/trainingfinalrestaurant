package com.trainingFinalRestaurant.trainingFinalRestaurant.service;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.PurchaseOrderDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.PurchaseOrder;

import java.util.List;

public interface PurchaseOrderService {
	List<PurchaseOrder> getPurchaseOrders();
	PurchaseOrder getPurchaseOrderById(int id);
	PurchaseOrder createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO);
}
