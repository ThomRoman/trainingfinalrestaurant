package com.trainingFinalRestaurant.trainingFinalRestaurant.service;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Dish;

import java.util.List;

public interface DishService {
	List<Dish> getDishes();
	Dish createDish(DishDTO dishDTO);
	Dish getDishById(int id);
}
