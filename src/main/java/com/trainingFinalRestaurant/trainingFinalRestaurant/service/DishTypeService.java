package com.trainingFinalRestaurant.trainingFinalRestaurant.service;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishTypeDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;

import java.util.List;

public interface DishTypeService {
	List<DishType> getDishTypes();
	DishType createDishType(DishTypeDTO dishTypeDTO);
	DishType getDishTypeById(int id);
}
