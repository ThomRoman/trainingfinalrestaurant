package com.trainingFinalRestaurant.trainingFinalRestaurant.service.impl;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishTypeDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.DishTypeRepository;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.DishTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishTypeServiceImpl implements DishTypeService {

	@Autowired
	private DishTypeRepository dishTypeRepository;

	@Override
	public List<DishType> getDishTypes() {
		return dishTypeRepository.getDishTypes();
	}

	@Override
	public DishType createDishType(DishTypeDTO dishTypeDTO) {
		return dishTypeRepository.createDishType(dishTypeDTO);
	}

	@Override
	public DishType getDishTypeById(int id) {
		return dishTypeRepository.getDishTypeById(id);
	}
}
