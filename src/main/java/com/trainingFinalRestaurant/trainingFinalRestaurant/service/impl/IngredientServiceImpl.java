package com.trainingFinalRestaurant.trainingFinalRestaurant.service.impl;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.IngredientDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.IngredientRepository;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService {


	@Autowired
	private IngredientRepository ingredientRepository;

	@Override
	public List<Ingredient> getIngredients() {
		var ingredients = ingredientRepository.getIngredients();
		for (Ingredient ingredient : ingredients){
			ingredientRepository.getIngredientById(ingredient.getId());
			var ingredientXDishes = ingredientRepository.ingredientXDishes(ingredient.getId());
			ingredient.setIngredientXDishes(ingredientXDishes);
		}
		return ingredients;
	}

	@Override
	public Ingredient createIngredient(IngredientDTO ingredientDTO) {
		return ingredientRepository.createIngredient(ingredientDTO);
	}

	@Override
	public Ingredient getIngredientById(int id) {
		var ingredient = ingredientRepository.getIngredientById(id);
		var ingredientXDishes = ingredientRepository.ingredientXDishes(id);
		ingredient.setIngredientXDishes(ingredientXDishes);
		return ingredient;
	}
}
