package com.trainingFinalRestaurant.trainingFinalRestaurant.service.impl;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Dish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.DishRepository;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishServiceImpl implements DishService {

	@Autowired
	private DishRepository dishRepository;

	@Override
	public List<Dish> getDishes() {
		var dishes = dishRepository.getDishes();
		for (Dish dish:dishes){
			var ingredients = dishRepository.getIngredientsFromDish(dish.getId());
			dish.setIngredientXDishes(ingredients);
		}
		return dishes;
	}

	@Override
	public Dish createDish(DishDTO dishDTO) {
		var dish =  dishRepository.createDish(dishDTO);
		var ingredients = dishRepository.getIngredientsFromDish(dish.getId());
		dish.setIngredientXDishes(ingredients);
		return dish;
	}

	@Override
	public Dish getDishById(int id) {
		var dish =  dishRepository.getDishById(id);
		var ingredients = dishRepository.getIngredientsFromDish(id);
		dish.setIngredientXDishes(ingredients);
		return dish;
	}
}
