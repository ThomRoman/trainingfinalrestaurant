package com.trainingFinalRestaurant.trainingFinalRestaurant.service.impl;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.PurchaseOrderDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.PurchaseOrder;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.PurchaseOrderService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {
	@Override
	public List<PurchaseOrder> getPurchaseOrders() {
		return null;
	}

	@Override
	public PurchaseOrder getPurchaseOrderById(int id) {
		return null;
	}

	@Override
	public PurchaseOrder createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) {
		return null;
	}
}
