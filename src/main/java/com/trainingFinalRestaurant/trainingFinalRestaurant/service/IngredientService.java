package com.trainingFinalRestaurant.trainingFinalRestaurant.service;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.IngredientDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;

import java.util.List;

public interface IngredientService {
	List<Ingredient> getIngredients();
	Ingredient createIngredient(IngredientDTO ingredientDTO);
	Ingredient getIngredientById(int id);
}
