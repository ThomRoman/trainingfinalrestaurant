package com.trainingFinalRestaurant.trainingFinalRestaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingFinalRestaurantApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingFinalRestaurantApplication.class, args);
	}

}
