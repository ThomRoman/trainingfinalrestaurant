package com.trainingFinalRestaurant.trainingFinalRestaurant.controller;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Dish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/dishes")
public class DishController {


	@Autowired
	private DishService dishService;

	@GetMapping("")
	public ResponseEntity<List<Dish>> getDishes(){
		try {
			var dishes = dishService.getDishes();
			return ResponseEntity.status(HttpStatus.OK).body(dishes);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping("")
	@Transactional
	public ResponseEntity<Dish> createDish(@RequestBody DishDTO dishDTO){
		try {
			var response = dishService.createDish(dishDTO);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<Dish> getDishById(@PathVariable(name = "id") Integer id){
		try {
			var dish = dishService.getDishById(id);
			return ResponseEntity.status(HttpStatus.OK).body(dish);
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			return ResponseEntity.internalServerError().build();
		}
	}
}
