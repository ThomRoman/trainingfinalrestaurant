package com.trainingFinalRestaurant.trainingFinalRestaurant.controller;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.PurchaseOrderDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.PurchaseOrder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/purchasesOrders")
public class PurchaseOrderController {
	@GetMapping("")
	public ResponseEntity<List<PurchaseOrder>> getDishTypes(){
		try {

			return ResponseEntity.status(HttpStatus.OK).body(null);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping("")
	@Transactional
	public ResponseEntity<PurchaseOrder> createDishType(@RequestBody PurchaseOrderDTO purchaseOrderDTO){
		try {

			return ResponseEntity.status(HttpStatus.OK).body(null);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<PurchaseOrder> getDishTypeById(@PathVariable(name = "id") Integer id){
		try {

			return ResponseEntity.status(HttpStatus.OK).body(null);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}
}
