package com.trainingFinalRestaurant.trainingFinalRestaurant.controller;


import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishTypeDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.DishTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/dishTypes")
public class DishTypeController {


	@Autowired
	private DishTypeService dishTypeService;

	@GetMapping("")
	public ResponseEntity<List<DishType>> getDishTypes(){
		try {

			return ResponseEntity.status(HttpStatus.OK).body(dishTypeService.getDishTypes());
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping("")
	@Transactional
	public ResponseEntity<DishType> createDishType(@RequestBody DishTypeDTO dishTypeDTO){
		try {
			var response = dishTypeService.createDishType(dishTypeDTO);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<DishType> getDishTypeById(@PathVariable(name = "id") Integer id){
		try {

			return ResponseEntity.status(HttpStatus.OK).body(dishTypeService.getDishTypeById(id));
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}
}
