package com.trainingFinalRestaurant.trainingFinalRestaurant.controller;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.IngredientDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/ingredients")
public class IngredientController {

	@Autowired
	private IngredientService ingredientService;

	@GetMapping("")
	public ResponseEntity<List<Ingredient>> getIngredients(){
		try {
			var response = ingredientService.getIngredients();
			return ResponseEntity.status(HttpStatus.OK).body(response);

		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping("")
	@Transactional
	public ResponseEntity<Ingredient> createIngredient(@RequestBody IngredientDTO ingredientDTO){
		try {
			var response = ingredientService.createIngredient(ingredientDTO);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<Ingredient> getIngredientById(@PathVariable(name = "id") Integer id){
		try {
			var response = ingredientService.getIngredientById(id);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}
}
