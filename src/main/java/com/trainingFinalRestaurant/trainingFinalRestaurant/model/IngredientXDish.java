package com.trainingFinalRestaurant.trainingFinalRestaurant.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IngredientXDish {
	private int id;
	private Ingredient ingredient;
	private Dish dish;

	private int quantity;
}
