package com.trainingFinalRestaurant.trainingFinalRestaurant.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Ingredient {
	private int id;
	private String name;
	private double ingredientPrice;

	private List<IngredientXDish> ingredientXDishes;
}
