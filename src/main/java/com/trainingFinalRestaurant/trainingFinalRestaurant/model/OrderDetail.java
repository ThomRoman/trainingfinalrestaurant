package com.trainingFinalRestaurant.trainingFinalRestaurant.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail {
	private int id;
	private PurchaseOrder purchaseOrder;
 	private Dish dish;
}
