package com.trainingFinalRestaurant.trainingFinalRestaurant.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrder {
	private int id;
	private Dish dish;
	private double totalPrice;

	private List<OrderDetail> orderDetails;
}
