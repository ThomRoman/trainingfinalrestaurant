package com.trainingFinalRestaurant.trainingFinalRestaurant.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Dish {
	private int id;
	private String name;
	private String description;
	private double dishPrice;
	private DishType dishType;

	private List<IngredientXDish> ingredientXDishes;


	private List<OrderDetail> orderDetails;
}
