package com.trainingFinalRestaurant.trainingFinalRestaurant.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DishType {
	private int id;
	private String name;
}
