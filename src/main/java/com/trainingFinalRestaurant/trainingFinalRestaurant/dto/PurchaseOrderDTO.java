package com.trainingFinalRestaurant.trainingFinalRestaurant.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrderDTO {
	private List<OrderDetailDTO> orderDetailDTOList;
}
