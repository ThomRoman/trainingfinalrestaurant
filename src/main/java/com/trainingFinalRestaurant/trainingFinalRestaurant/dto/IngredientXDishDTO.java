package com.trainingFinalRestaurant.trainingFinalRestaurant.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class IngredientXDishDTO {
	private int idIngredient;
	private int quantity;
}
