package com.trainingFinalRestaurant.trainingFinalRestaurant.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DishDTO {
	private String name;
	private String description;
	private int idDishType;
	private List<IngredientXDishDTO> ingredientsDTO;
}
