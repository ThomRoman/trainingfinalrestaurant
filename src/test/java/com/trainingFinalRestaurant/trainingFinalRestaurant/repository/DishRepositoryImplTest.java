package com.trainingFinalRestaurant.trainingFinalRestaurant.repository;


import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Dish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.IngredientXDish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.impl.DishRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishRepositoryImplTest {

	@Mock
	private JdbcTemplate jdbcTemplate;

	@InjectMocks
	private DishRepositoryImpl dishRepository;

	@Test
	public void getDishesTest(){
		List<Dish> list = List.of(new Dish());
		given(jdbcTemplate.query(anyString(), any(RowMapper.class))).willReturn(list);
		var response = dishRepository.getDishes();
		assertEquals(list,response);
	}

	@Test
	public void mapToDishTest() throws SQLException {

		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getDouble("dishPrice")).thenReturn(5.2);
		when(resultSet.getInt("dish_id")).thenReturn(1);
		when(resultSet.getString("name")).thenReturn("name");
		when(resultSet.getString("description")).thenReturn("description");
		when(resultSet.getInt("idDishType")).thenReturn(1);
		when(resultSet.getString("dish_type_name")).thenReturn("dish_type_name");

		var response = dishRepository.mapToDish(resultSet);
		assertEquals(5.2,response.getDishPrice());
		assertEquals("dish_type_name",response.getDishType().getName());
	}

	@Test
	public void createDishTest(){
		DishDTO dishDTO = new DishDTO();
		Dish dish = new Dish();
		given(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class),any())).willReturn(dish);

		var response = dishRepository.createDish(dishDTO);

		assertNotNull(response);
		assertEquals(dish,response);
	}

	@Test
	public void createDishErrorTest(){
		DishDTO dishDTO = new DishDTO();
		Dish dish = new Dish();
		given(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class),any())).willThrow(new NullPointerException("error"));
		var response = dishRepository.createDish(dishDTO);
		assertNull(response);
	}

	@Test
	public void mapToDishForProcedureTest() throws SQLException{
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getInt("idDishType")).thenReturn(1);
		when(resultSet.getString("dish_type_name")).thenReturn("dish_type_name");
		when(resultSet.getDouble("dishPrice")).thenReturn(20.5);
		when(resultSet.getInt("dish_id")).thenReturn(1);
		when(resultSet.getString("dish_name")).thenReturn("asd");
		when(resultSet.getString("description")).thenReturn("dish_type_name");

		var response = dishRepository.mapToDishForProcedure(resultSet);
		assertNotNull(response);

		assertEquals(20.5,response.getDishPrice());
	}

	@Test
	public void getIngredientsFromDishTest(){
		List<IngredientXDish> ingredientXDishes = List.of(new IngredientXDish());
		given(jdbcTemplate.query(anyString(), any(RowMapper.class),anyInt())).willReturn(ingredientXDishes);
		var response = dishRepository.getIngredientsFromDish(1);
		assertNotNull(response);
		assertEquals(ingredientXDishes,response);
	}

	@Test
	public void mapToIngredientsFromDish() throws SQLException{
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getInt("ixd_id")).thenReturn(1);
		when(resultSet.getInt("quantity")).thenReturn(20);
		when(resultSet.getInt("idIngredient")).thenReturn(1);
		when(resultSet.getString("ingredient_name")).thenReturn("asdasd");
		when(resultSet.getDouble("ingredientPrice")).thenReturn(1.5);
		var response = dishRepository.mapToIngredientsFromDish(resultSet);
		assertNotNull(response);
		assertEquals(1.5,response.getIngredient().getIngredientPrice());
	}

	@Test
	public void getDishById(){
		Dish d = new Dish();
		given(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class),anyInt())).willReturn(d);
		var response = dishRepository.getDishById(1);
		assertNotNull(response);
		assertEquals(d,response);
	}
}
