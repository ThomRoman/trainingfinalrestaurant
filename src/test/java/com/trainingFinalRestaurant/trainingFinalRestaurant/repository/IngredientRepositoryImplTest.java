package com.trainingFinalRestaurant.trainingFinalRestaurant.repository;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.IngredientDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.IngredientXDish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.impl.IngredientRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IngredientRepositoryImplTest {
	@Mock
	private JdbcTemplate jdbcTemplate;

	@InjectMocks
	private IngredientRepositoryImpl ingredientRepository;

	@Test
	public void getIngredientsTest(){
		List<Ingredient> list = List.of(new Ingredient());
		given(jdbcTemplate.query(anyString(), any(RowMapper.class))).willReturn(list);
		var response = ingredientRepository.getIngredients();
		assertEquals(list,response);
	}

	@Test
	public void createIngredientTest(){
		var ingredient = new Ingredient();
		ingredient.setId(1);
		ingredient.setName("ingredient");

		given(jdbcTemplate.queryForObject(anyString(),any(),any(RowMapper.class))).willReturn(ingredient);
		var response = ingredientRepository.createIngredient(new IngredientDTO());
		assertNotNull(response);
		assertEquals(ingredient,response);
	}

	@Test
	public void mapToIngredient() throws SQLException {
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getInt("id")).thenReturn(1);
		when(resultSet.getString("name")).thenReturn("bebidas");
		when(resultSet.getInt("ingredientPrice")).thenReturn(1);

		var response = ingredientRepository.mapToIngredient(resultSet);
		assertNotNull(response);
		assertEquals("bebidas",response.getName());
		assertEquals(1,response.getId());
	}

	@Test
	public void getIngredientById(){
		Ingredient ingredient = new Ingredient();
		given(jdbcTemplate.queryForObject(anyString(),any(RowMapper.class),anyInt())).willReturn(ingredient);
		var response = ingredientRepository.getIngredientById(1);
		assertNotNull(response);
		assertEquals(ingredient,response);
	}

	@Test
	public void ingredientXDishes(){
		List<IngredientXDish> ixds = List.of(new IngredientXDish());
		given(jdbcTemplate.query(anyString(), any(RowMapper.class),anyInt())).willReturn(ixds);
		var response = ingredientRepository.ingredientXDishes(1);
		assertNotNull(response);
		assertEquals(ixds,response);
	}

	@Test
	public void maptoIngredientXDish() throws SQLException{
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getInt("idDish")).thenReturn(1);
		when(resultSet.getString("dish_name")).thenReturn("bebidas");
		when(resultSet.getDouble("dish_price")).thenReturn(10.2);
		when(resultSet.getString("dish_description")).thenReturn("asdas");
		when(resultSet.getInt("ixd_id")).thenReturn(1);
		when(resultSet.getInt("quantity")).thenReturn(10);

		var response = ingredientRepository.maptoIngredientXDish(resultSet);

		assertNotNull(response);
		assertEquals(10.2,response.getDish().getDishPrice());

	}
}
