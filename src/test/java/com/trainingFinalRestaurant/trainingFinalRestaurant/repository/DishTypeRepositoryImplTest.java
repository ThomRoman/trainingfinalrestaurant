package com.trainingFinalRestaurant.trainingFinalRestaurant.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishTypeDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.impl.DishTypeRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class DishTypeRepositoryImplTest {
	@Mock
	private JdbcTemplate jdbcTemplate;

	@InjectMocks
	private DishTypeRepositoryImpl dishTypeRepository;


	@Test
	public void getDishTypesTest(){
		List<DishType> list = List.of(new DishType());
		given(jdbcTemplate.query(anyString(), any(RowMapper.class))).willReturn(list);
		var response = dishTypeRepository.getDishTypes();
		assertEquals(list,response);
	}

	@Test
	public void createDishTypeTest(){
		DishType dishType = new DishType();
		given(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class),anyString())).willReturn(dishType);

		var response = dishTypeRepository.createDishType(new DishTypeDTO("a"));
		assertNotNull(response);
		assertEquals(dishType,response);
	}

	@Test
	public void getDishTypeByIdTest(){
		DishType dishType = new DishType();
		given(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class),anyInt())).willReturn(dishType);
		var response = dishTypeRepository.getDishTypeById(1);
		assertNotNull(response);
		assertEquals(dishType,response);
	}


	@Test
	public void mapToDishTypeTest() throws SQLException {
		DishType dishType = new DishType(1,"bebidas");
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getInt("id")).thenReturn(1);
		when(resultSet.getString("name")).thenReturn("bebidas");

		var fetched = dishTypeRepository.mapToDishType(resultSet);
		assertNotNull(fetched);
		assertEquals(dishType.getName(), fetched.getName());
		assertEquals(dishType.getId(), fetched.getId());
	}

}
