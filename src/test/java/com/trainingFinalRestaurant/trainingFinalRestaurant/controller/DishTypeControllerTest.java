package com.trainingFinalRestaurant.trainingFinalRestaurant.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trainingFinalRestaurant.trainingFinalRestaurant.controller.DishTypeController;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.DishTypeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;
import java.util.List;

import static org.mockito.BDDMockito.given;
@WebMvcTest(DishTypeController.class)
public class DishTypeControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private DishTypeService dishTypeService;

	@Test
	public void getDishTypes() throws Exception {
		List<DishType> typeList = List.of();
		given(dishTypeService.getDishTypes()).willReturn(typeList);

		ResultActions response = mockMvc.perform(get("/v1/dishTypes"));
		response.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$.size()",is(typeList.size())));
	}
}
