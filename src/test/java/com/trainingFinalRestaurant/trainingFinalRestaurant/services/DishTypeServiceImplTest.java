package com.trainingFinalRestaurant.trainingFinalRestaurant.services;


import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishTypeDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.DishType;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.DishTypeRepository;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.impl.DishTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import org.mockito.Mock;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class DishTypeServiceImplTest {

	@Mock
	private DishTypeRepository dishTypeRepository;

	@InjectMocks
	private DishTypeServiceImpl dishTypeService;


	@Test
	public void getDishTypesTest(){
		List<DishType> dishTypes = List.of();
		when(dishTypeRepository.getDishTypes()).thenReturn(dishTypes);
		var response = dishTypeService.getDishTypes();
		assertNotNull(response);
		assertEquals(dishTypes,response);
	}

	@Test
	public void createDishTypeTest(){
		DishType dishType = new DishType();
		when(dishTypeRepository.createDishType(any(DishTypeDTO.class))).thenReturn(dishType);
		var response = dishTypeService.createDishType(new DishTypeDTO());
		assertNotNull(response);
		assertEquals(dishType,response);
	}

	@Test
	public void getDishTypeByIdTest(){

		DishType dishType = new DishType();
		when(dishTypeRepository.getDishTypeById(anyInt())).thenReturn(dishType);

		var response = dishTypeService.getDishTypeById(1);
		assertNotNull(response);
		assertEquals(dishType,response);
	}
}
