package com.trainingFinalRestaurant.trainingFinalRestaurant.services;


import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.IngredientDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.IngredientXDish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.IngredientRepository;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.impl.IngredientServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IngredientServiceTest {

	@Mock
	private IngredientRepository ingredientRepository;


	@InjectMocks
	private IngredientServiceImpl ingredientService;

	@Test
	public void getIngredientsTest(){
		Ingredient ingredient = new Ingredient();
		List<IngredientXDish> ingredientXDishes = List.of();
		ingredient.setId(1);
		List<Ingredient> list = List.of(ingredient);
		when(ingredientRepository.getIngredients()).thenReturn(list);
		when(ingredientRepository.getIngredientById(anyInt())).thenReturn(ingredient);
		when(ingredientRepository.ingredientXDishes(anyInt())).thenReturn(ingredientXDishes);

		var response =  ingredientService.getIngredients();

		assertNotNull(response);
		assertEquals(list,response);
	}

	@Test
	public void createIngredient(){
		Ingredient ingredient = new Ingredient();
		when(ingredientRepository.createIngredient(any(IngredientDTO.class))).thenReturn(ingredient);
		var response =  ingredientService.createIngredient(new IngredientDTO());

		assertNotNull(response);
		assertEquals(ingredient,response);
	}

	@Test
	public void getIngredientById(){

		Ingredient ingredient = new Ingredient();
		List<IngredientXDish> ingredientXDishes = List.of();

		when(ingredientRepository.getIngredientById(anyInt())).thenReturn(ingredient);
		when(ingredientRepository.ingredientXDishes(anyInt())).thenReturn(ingredientXDishes);

		var response =  ingredientService.getIngredientById(1);

		assertNotNull(response);
		assertEquals(ingredient,response);
	}
}
