package com.trainingFinalRestaurant.trainingFinalRestaurant.services;


import com.trainingFinalRestaurant.trainingFinalRestaurant.dto.DishDTO;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Dish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.Ingredient;
import com.trainingFinalRestaurant.trainingFinalRestaurant.model.IngredientXDish;
import com.trainingFinalRestaurant.trainingFinalRestaurant.repository.DishRepository;
import com.trainingFinalRestaurant.trainingFinalRestaurant.service.impl.DishServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishServiceImplTest {

	@Mock
	private DishRepository dishRepository;


	@InjectMocks
	private DishServiceImpl dishService;


	@Test
	public void getDishes(){
		Dish dish = new Dish();
		dish.setId(1);
		IngredientXDish i = new IngredientXDish();
		i.setQuantity(10);
		List<IngredientXDish> ingredientXDishes = List.of(i);
		List<Dish> dishes = List.of(dish);
		when(dishRepository.getDishes()).thenReturn(dishes);
		when(dishRepository.getIngredientsFromDish(anyInt())).thenReturn(ingredientXDishes);

		var response = dishService.getDishes();

		assertEquals(10,response.get(0).getIngredientXDishes().get(0).getQuantity());
	}


	@Test
	public void createDish(){
		Dish dish = new Dish();
		dish.setId(1);
		IngredientXDish i = new IngredientXDish();
		i.setQuantity(10);
		List<IngredientXDish> ingredientXDishes = List.of(i);
		when(dishRepository.createDish(any(DishDTO.class))).thenReturn(dish);
		when(dishRepository.getIngredientsFromDish(anyInt())).thenReturn(ingredientXDishes);

		var response = dishService.createDish(new DishDTO());

		assertEquals(dish,response);
	}

	@Test
	public void getDishById(){
		Dish dish = new Dish();
		dish.setId(1);
		IngredientXDish i = new IngredientXDish();
		i.setQuantity(10);
		List<IngredientXDish> ingredientXDishes = List.of(i);
		when(dishRepository.getDishById(anyInt())).thenReturn(dish);
		when(dishRepository.getIngredientsFromDish(anyInt())).thenReturn(ingredientXDishes);

		var response = dishService.getDishById(1);

		assertEquals(dish,response);
	}
}
